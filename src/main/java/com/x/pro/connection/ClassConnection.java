package com.x.pro.connection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:application.properties")
public class ClassConnection {
	
	@Value("${esquema}")
	private String esquema;
	
	public Sql2o connection;
	
	public ClassConnection(
			@Value("${spring.datasource.url}") String urlConexion
			, @Value("${spring.datasource.username}") String user
			, @Value("${spring.datasource.password}") String password) {
		this.connection = new Sql2o(urlConexion, user, password);
	}
	
	public String getEsquema() {
		return esquema;
	}
	
	@SuppressWarnings("deprecation")
	public Query createQueryO2P(String queryText) {
		return this.connection.createQuery("SELECT * FROM \""+ this.esquema +"\"." + queryText);
	}
}
