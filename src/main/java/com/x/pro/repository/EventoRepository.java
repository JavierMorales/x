package com.x.pro.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import com.x.pro.model.Evento;
import com.x.pro.service.IEventoService;

@Service
@PropertySource("classpath:application.properties")
public class EventoRepository implements IEventoService {
	
	@Value("${spring.datasource.url}")
	private String urlConexion;
	
	@Value("${spring.datasource.username}")
	private String user;
	
	@Value("${spring.datasource.password}")
	private String password;

	@Override
	public List<Evento> ObtieneLista(Evento obj){
		List<Evento> Eventos = new ArrayList<Evento>();
		Sql2o sql2o = new Sql2o(urlConexion,user,password);
		try (Connection connection = sql2o.open()) {
			String query = "SELECT idevento, folio, observaciones FROM public.\"EVENTO\";";
			Eventos = connection
					.createQuery(query)
					.executeAndFetch(Evento.class);
			if( connection != null)
				connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return Eventos;
	}

	@Override
	public List<Evento> Obtiene(Evento obj) {
		List<Evento> Evento = new ArrayList<Evento>();
		try (Connection connection = new Sql2o(urlConexion,user,password).open()) {
			String query = "SELECT idevento, folio, observaciones FROM public.\"EVENTO\" WHERE (idevento = :idEvento OR :idEvento IS NULL);";
			Evento = connection
					.createQuery(query)
					.addParameter("idEvento", obj.idEvento)
					.executeAndFetch(Evento.class);
			if( connection != null)
				connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return Evento;
	}

}
