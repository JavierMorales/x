package com.x.pro.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.sql2o.Connection;

import com.x.pro.connection.ClassConnection;
import com.x.pro.model.Usuario;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class UsuarioRepository {
	
	@Autowired
	ClassConnection conn;

	public Mono<Usuario> Obtiene(Usuario obj) {
		List<Usuario> usuario = new ArrayList<Usuario>();
		try (Connection connection = this.conn.connection.open()) {
			usuario = this.conn.createQueryO2P("TELEFONISTA_USUARIOS_GET(:centro,:cuenta,:password);")
					.addParameter("centro", obj.centro)
					.addParameter("cuenta", obj.cuenta)
					.addParameter("password", obj.password)
					.executeAndFetch(Usuario.class);
		} catch (Exception e) {
			System.out.println(e);
		}
		return Mono.just(usuario.get(0));
	}
}
