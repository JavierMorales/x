package com.x.pro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.x.pro.model.Usuario;
import com.x.pro.repository.UsuarioRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;

@Api()
@CrossOrigin()
@RestController(value = "Usuario")
@RequestMapping("/api/usuario")
public class UsuarioController<T> {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@PostMapping("/obtiene")
	public Mono<ResponseEntity<T>> Obtiene(
			@ApiParam(value = "JSON de objeto prueba", required = true) @Validated @RequestBody Usuario obj) {
		try {
			return this.usuarioRepository.Obtiene(obj)
					.map(response -> new ResponseEntity<T>((T) response, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<T>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(response -> new ResponseEntity<T>((T) response, HttpStatus.INTERNAL_SERVER_ERROR));
			
		}
	}
}
