package com.x.pro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.x.pro.model.Evento;
import com.x.pro.service.IEventoService;

import io.swagger.annotations.*;

@Api()
@CrossOrigin()
@RestController(value = "Evento")
@RequestMapping("/api/evento")
public class EventoController {

	@Autowired
	IEventoService iEventoService;

	@PostMapping("/obtieneLista")
	public List<Evento> ObtieneLista(
			@ApiParam(value = "JSON de objeto prueba", required = true) @Validated @RequestBody Evento obj) {
		return this.iEventoService.ObtieneLista(obj);
	}
	
	@PostMapping("/obtiene")
	public List<Evento> Obtiene(
			@ApiParam(value = "JSON de objeto prueba", required = true) @Validated @RequestBody Evento obj) {
		return this.iEventoService.Obtiene(obj);
	}
}
