package com.x.pro.model;

public class Usuario {
	public String uuid;
	public String id_usuario;
	public String nombre_usuario;
	public String apellido_paterno;
	public String cuenta;
	public String habilitado;
	public String id_rol_usuario;
	public String id_rol;
	public String nombre;
	public String descripcion;
	
	public String centro;
	public String password;
	public String esquema;
}
