package com.x.pro.service;

import java.util.List;

import com.x.pro.model.Evento;

public interface IEventoService {

	public List<Evento> ObtieneLista(Evento obj);
	
	public List<Evento> Obtiene(Evento obj);
}
